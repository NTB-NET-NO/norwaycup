<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="xml" encoding="ISO-8859-1" indent="yes"/>

<xsl:template match="/">
<xsl:text>&#13;&#10;</xsl:text>
<nitf version='-//IPTC-NAA//DTD NITF-XML 3.2//EN' change.date='October 10, 2003' change.time='19:30' baselang='no-NO'>
	<xsl:apply-templates/>
<xsl:text>&#13;&#10;</xsl:text>
</nitf>	
</xsl:template>

<xsl:template match="head">
	<xsl:copy-of select="."/>
</xsl:template>

<xsl:template match="body">
<body>
<xsl:apply-templates/>
</body>
</xsl:template>

<xsl:template match="body.head">
	<xsl:copy-of select="."/>
</xsl:template>

<xsl:template match="body.content">
<body.content>
<xsl:apply-templates/>
</body.content>	
</xsl:template>

<xsl:template match="body.end">
	<xsl:copy-of select="."/>
</xsl:template>

<xsl:template match="hl2">
	<xsl:copy-of select="."/>
</xsl:template>

<xsl:template match="p">
	<xsl:copy-of select="."/>
</xsl:template>

<xsl:template match="table">
	<xsl:copy-of select="."/>
</xsl:template>

<xsl:template match="table[@class='9-col']" />

<xsl:template match="p[.='[t01u5]']" />

<xsl:template match="p[.='']" />

</xsl:stylesheet>