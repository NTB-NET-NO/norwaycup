<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output indent="yes" method="text"/>

<xsl:variable name="lf">{crlf}</xsl:variable>

<xsl:template name="fipheader">
	<xsl:text>~</xsl:text>
	<xsl:value-of select="$lf"/>
	<xsl:text>SU:NTAOUT</xsl:text>
	<xsl:value-of select="$lf"/>
	<xsl:text>SN:NTAOUT</xsl:text>
	<xsl:value-of select="substring(head/pubdata/@date.publication , 11, 5)"/>
	<xsl:value-of select="$lf"/>
	<xsl:text>DB:NTAOUT</xsl:text>
	<xsl:value-of select="$lf"/>
	<xsl:text>SA:</xsl:text>
	<xsl:value-of select="$lf"/>
	<xsl:text>PC:</xsl:text>
	<xsl:value-of select="substring(head/pubdata/@date.publication , 5, 4)"/>
	<xsl:value-of select="substring(head/pubdata/@date.publication , 10, 6)"/>
	<xsl:value-of select="$lf"/>
	<xsl:text>DI:NTB</xsl:text>
	<xsl:value-of select="$lf"/>
	<xsl:text>DU:satftpa</xsl:text>
	<xsl:value-of select="$lf"/>
	<xsl:text>ZN:SU.NTAOUT.DU.satftpa.SN.NTAOUT</xsl:text>
	<xsl:value-of select="substring(head/pubdata/@date.publication , 11, 5)"/>	
	<xsl:value-of select="$lf"/>
	<xsl:value-of select="$lf"/>
	<xsl:text>DQ:satftpa</xsl:text>
	<xsl:value-of select="$lf"/>
	<xsl:value-of select="$lf"/>
	<xsl:text>DU:satftpa</xsl:text>
	<xsl:value-of select="$lf"/>
	<xsl:value-of select="$lf"/>
	<xsl:value-of select="$lf"/>
	<xsl:text>~</xsl:text>
	<xsl:value-of select="$lf"/>
</xsl:template>

<!-- Fipheader exsempel:
~
SU:NTAOUT
SN:NTAOUT16254
DB:NTAOUT
SA:
PC:0871E0190D
DI:ALL
DU:satftpa
ZN:SU.NTAOUT.DU.satftpa.SN.NTAOUT16254

DQ:satftpa

DU:satftpa


~ 
-->

</xsl:stylesheet>
