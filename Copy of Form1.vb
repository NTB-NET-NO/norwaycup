Imports System.IO
Imports System.Net
Imports System.Text
Imports System.Configuration.ConfigurationSettings
Imports System.Xml
Imports System.Web.HttpUtility
Imports NTB_FuncLib2

Public Class Form1_old
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(8, 48)
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.TextBox1.Size = New System.Drawing.Size(416, 584)
        Me.TextBox1.TabIndex = 1
        Me.TextBox1.Text = ""
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(432, 48)
        Me.TextBox2.Multiline = True
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.TextBox2.Size = New System.Drawing.Size(472, 584)
        Me.TextBox2.TabIndex = 3
        Me.TextBox2.Text = ""
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(8, 16)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(120, 23)
        Me.Button4.TabIndex = 5
        Me.Button4.Text = "Hent URLGrunnspill"
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(144, 16)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(112, 23)
        Me.Button5.TabIndex = 6
        Me.Button5.Text = "Hent URLSluttspill"
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(912, 638)
        Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.Button5, Me.Button4, Me.TextBox2, Me.TextBox1})
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)

    End Sub

#End Region


#Region " Service wide vars "

    '    Const URL1 As String = "http://www.n3sport.no/sportsdata/sportsdata.asp?WCI=wiDownload&state=Tournament&id=1771&span=&tableonly="
    Const URLGrunnspill As String = "http://195.139.49.25/external/sportsdata/sportsdata.asp?WCI=wiDownload&state=job&id=1748&span=500&tableonly="
    Const URLSluttspill As String = "http://195.139.49.25/external/sportsdata/sportsdata.asp?WCI=wiDownload&state=job&id=1749&span=500&tableonly="

    'Folders
    Dim logFiles As String
    Dim queryLog As String
    Dim xmlOut As String

    'Files
    Dim versionFile As String

    'Logfiles
    Dim systemLog As StreamWriter
    Dim errorLog As StreamWriter

    'Query
    Dim server As String

    Dim versionNr As Integer 'Last Run ID

    'Other
    Dim XmlDoc As XmlDocument 'Reusable XML doc

#End Region

    Private Sub Form1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Init("grunnspill")
        RunJob("grunnspill")
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        '       Me.TextBox1.Text = GetUrlResponse(URL1)
    End Sub

    'Load settings and initiate server
    Private Sub Init(ByVal type As String)
        'Init vars
        XmlDoc = New XmlDocument()

        'Load settings
        logFiles = AppSettings("logFileFolder")
        queryLog = AppSettings("queryLogFolder")
        xmlOut = AppSettings("xmlOutFolder")
        versionFile = AppSettings("versionFile")

        Directory.CreateDirectory(logFiles)
        Directory.CreateDirectory(queryLog)
        Directory.CreateDirectory(xmlOut)
        Directory.CreateDirectory(Path.GetDirectoryName(versionFile))

        If Not File.Exists(versionFile) Then
            Dim strVersionDoc As String
            strVersionDoc = "<?xml version='1.0' encoding='iso-8859-1' standalone='yes'?>" & vbCrLf _
                    & "<doc>" & vbCrLf _
                    & "<grunnspill>" & vbCrLf _
                    & "<last-run>2003.01.01</last-run>" & vbCrLf _
                    & "<run-count>0</run-count>" & vbCrLf _
                    & "</grunnspill>" & vbCrLf _
                    & "<sluttspill>" & vbCrLf _
                    & "<last-run>2003.01.01</last-run>" & vbCrLf _
                    & "<run-count>0</run-count>" & vbCrLf _
                    & "</sluttspill>" & vbCrLf _
                    & "</doc>" & vbCrLf
            LogFile.WriteFile(versionFile, strVersionDoc)
            versionNr = 0
        Else
            XmlDoc.Load(versionFile)
            Dim nodeCount As XmlNode = XmlDoc.SelectSingleNode("/doc/" & type & "/run-count")
            Dim nodeLast As XmlNode = XmlDoc.SelectSingleNode("/doc/" & type & "/last-run")
            If nodeLast.InnerText = Format(Today, "yyyy.MM.dd") Then
                versionNr = nodeCount.InnerText + 1
                nodeCount.InnerText = versionNr
            Else
                nodeCount.InnerText = 0
                nodeLast.InnerText = Format(Today, "yyyy.MM.dd")
                versionNr = 0
            End If
        End If

        'Set up logging
        systemLog = New StreamWriter(logFiles & "system-log.txt", True, Encoding.GetEncoding("iso-8859-1"))
        systemLog.AutoFlush = True

        'Log event
        systemLog.WriteLine(Now & " : -- ValgQuery Service: Logging started --")

        'Query settings
        server = AppSettings("queryServer")

    End Sub

    Private Function GetUrlResponse(ByVal URL As String) As String

        Dim ok As Boolean = True
        Dim ret As String

        'Create the URI
        Dim uri As New UriBuilder(URL)

        'Create the HTTP request
        Dim request As HttpWebRequest = WebRequest.Create(uri.Uri)
        request.Method = "GET"
        request.Timeout = 60000


        'Get the HTTP response with the resulting data
        Dim response As HttpWebResponse
        Try
            response = request.GetResponse()
        Catch webEx As WebException
            'Log WEB error
            ok = False
            LogError(webEx)
            systemLog.WriteLine(Now & " : HTTP request failed: " & webEx.Message)
            If Not webEx.Response Is Nothing Then webEx.Response.Close()
        End Try

        'Read the data
        If ok Then
            Dim reader As StreamReader = New StreamReader(response.GetResponseStream(), Encoding.GetEncoding("iso-8859-1"))
            ret = reader.ReadToEnd
            reader.Close()
        End If

        Return ret
    End Function

    'Logs an exception to error-log.txt
    Sub LogError(ByVal ex As Exception)

        Try
            errorLog = New StreamWriter(logFiles & "error-log.txt", True, Encoding.GetEncoding("iso-8859-1"))
            errorLog.WriteLine(Now & " : " & ex.Message)
            errorLog.WriteLine(ex.StackTrace & vbCrLf & vbCrLf)
            errorLog.Close()
        Catch e As Exception
        End Try

    End Sub

    Private Function LagNitf(ByVal strDoc As String, ByVal strHedline As String, ByVal keyWord As String, ByVal versjon As Integer) As String
        Dim arrDoc() As String = Split(strDoc, vbCrLf)
        Dim strTableType As String = ""

        Dim strNy As String = ""
        If versjon > 0 Then
            strNy = "ny" & versjon & "-"
        End If

        Dim sbDoc As New StringBuilder()

        sbDoc.Append("<?xml version='1.0' encoding='iso-8859-1' standalone='yes'?>" & vbCrLf)
        sbDoc.Append("<nitf version='-//IPTC-NAA//DTD NITF-XML 2.5//EN' change.date='9 august 2000' change.time='1900' baselang='no-NO'>" & vbCrLf)

        sbDoc.Append("<head>" & vbCrLf)
        sbDoc.Append("<title>Resultater Norway Cup</title>" & vbCrLf)
        sbDoc.Append("<meta name=""ntb-folder"" content=""Ut-Satellitt"" />" & vbCrLf)
        sbDoc.Append("<meta name=""ntb-date"" content=""" & Format(Now, "dd.MM.yyyy HH:mm") & """ />" & vbCrLf)
        sbDoc.Append("<meta name=""ntb-filename"" content=""0401E017B6.xml"" />" & vbCrLf)
        sbDoc.Append("<meta name=""ntb-distribusjonsKode"" content=""ALL"" />" & vbCrLf)
        sbDoc.Append("<meta name=""ntb-kanal"" content=""A"" />" & vbCrLf)
        sbDoc.Append("<meta name=""ntb-meldingsSign"" content=""NC"" />" & vbCrLf)
        sbDoc.Append("<meta name=""ntb-bilderAntall"" content=""0"" />" & vbCrLf)
        sbDoc.Append("<meta name=""ntb-id"" content=""RED" & Format(Now, "yyyyMMddHHmmss") & "_nc"" />" & vbCrLf)
        sbDoc.Append("<meta name=""ntb-meldingsVersjon"" content=""" & versjon & """ />" & vbCrLf)

        sbDoc.Append("<docdata>" & vbCrLf)
        sbDoc.Append("<evloc county-dist=""Oslo;Riksnyheter;"">norge;</evloc>" & vbCrLf)
        sbDoc.Append("<doc-id regsrc=""NTB"" id-string=""RED" & Format(Now, "yyyyMMddHHmmss") & "_nc_0" & versjon & """ />" & vbCrLf)
        sbDoc.Append("<urgency ed-urg=""5"" />" & vbCrLf)
        sbDoc.Append("<date.issue norm=""" & Format(Now, "yyyyMMdd") & """ />" & vbCrLf)
        sbDoc.Append("<ed-msg info="""" />" & vbCrLf)
        sbDoc.Append("<du-key version=""" & versjon + 1 & """ key=""" & strNy & keyWord & """ />" & vbCrLf)
        sbDoc.Append("<doc.copyright year=""" & Format(Now, "yyyy") & """ holder=""NTB"" />" & vbCrLf)
        sbDoc.Append("<key-list>" & vbCrLf)
        sbDoc.Append("<keyword key=""" & keyWord & """ />" & vbCrLf)
        sbDoc.Append("</key-list>" & vbCrLf)
        sbDoc.Append("</docdata>" & vbCrLf)

        sbDoc.Append("<pubdata date.publication=""" & Format(Now, "yyyyMMddTHHmmss") & """ item-length=""1768"" unit-of-measure=""character"" />" & vbCrLf)
        sbDoc.Append("<revision-history name=""nwc"" />" & vbCrLf)
        sbDoc.Append("<tobject tobject.type=""Sport"">" & vbCrLf)
        sbDoc.Append("<tobject.property tobject.property.type=""Tabeller og resultater"" />" & vbCrLf)
        sbDoc.Append("<tobject.subject tobject.subject.refnum=""15000000"" tobject.subject.code=""SPO"" tobject.subject.type=""Sport"" tobject.subject.matter=""fotball;"" />" & vbCrLf)
        sbDoc.Append("</tobject>" & vbCrLf)
        sbDoc.Append("</head>" & vbCrLf)

        sbDoc.Append("<body>" & vbCrLf)

        sbDoc.Append("<body.head>" & vbCrLf)
        sbDoc.Append("<distributor>NTB</distributor>" & vbCrLf)
        sbDoc.Append("<hedline>" & vbCrLf)
        sbDoc.Append("<hl1>" & strHedline & "</hl1>" & vbCrLf)
        sbDoc.Append("</hedline>" & vbCrLf)
        sbDoc.Append("</body.head>" & vbCrLf)
        sbDoc.Append("<body.content>" & vbCrLf)

        Dim firstLine As Boolean = True
        Dim bTable As Boolean = False

        Dim line As String
        For Each line In arrDoc
            line = getXmlEntity(line)
            If line.IndexOf(vbTab) = -1 Then
                ' Vanlige linjer (ikke tabell)
                If bTable Then
                    bTable = False
                    sbDoc.Append("</table>" & vbCrLf)
                End If
                If firstLine Then
                    ' F�rste linje blir ingress:
                    If line <> "" Then
                        sbDoc.Append("<p lede=""true"">[t01]</p>" & vbCrLf)
                        ' Hopper over f�rste linje
                        'sbDoc.Append("<p lede=""true"">" & line & "</p>" & vbCrLf)
                        'sbDoc.Append("<p>" & line & "</p>" & vbCrLf)
                        firstLine = False
                    End If
                ElseIf line.StartsWith("Norway Cup ") Or line.StartsWith("Klasse ") Then
                    sbDoc.Append("<hl2>" & line & "</hl2>" & vbCrLf)
                Else
                    sbDoc.Append("<p>" & line & "</p>" & vbCrLf)
                End If
            Else
                ' Tabell Linjer (inneholder tab)
                Dim arrLine() As String = Split(line, vbTab)
                Dim col As String
                Dim firstCol As Boolean = True


                If Not bTable Then
                    If arrLine.Length = 4 Then
                        strTableType = "t01u2"
                    ElseIf arrLine.Length = 9 Then
                        strTableType = "t01u5"
                    Else
                        strTableType = "t01u" & arrLine.Length
                    End If

                    sbDoc.Append("<p style=""tabellkode"">[" & strTableType & "]</p>")
                    'sbDoc.Append("<table class=""" & strTableType & """ border=""1""  cellspacing=""0"" cellpadding=""3"">" & vbCrLf)
                    sbDoc.Append("<table class=""" & strTableType & """>" & vbCrLf)
                    bTable = True
                End If

                ' Ny linje
                sbDoc.Append("<tr>")
                For Each col In arrLine
                    ' Kolonner i tabell
                    If firstCol Then
                        sbDoc.Append("<td>")
                        firstCol = False
                    ElseIf col = "-" Then
                        sbDoc.Append("<td align='center'>")
                    Else
                        sbDoc.Append("<td align='right'>")
                    End If

                    sbDoc.Append(col)
                    sbDoc.Append("</td>")
                Next
                sbDoc.Append("</tr>" & vbCrLf)
            End If
        Next

        If bTable Then
            bTable = False
            sbDoc.Append("</table>" & vbCrLf)
        End If

        sbDoc.Append("</body.content>" & vbCrLf)

        sbDoc.Append("<body.end>" & vbCrLf)
        sbDoc.Append("<tagline>" & vbCrLf)
        sbDoc.Append("<a href=""mailto:sporten@ntb.no"">sporten@ntb.no</a>" & vbCrLf)
        sbDoc.Append("</tagline>" & vbCrLf)
        sbDoc.Append("</body.end>" & vbCrLf)

        sbDoc.Append("</body>" & vbCrLf)
        sbDoc.Append("</nitf>" & vbCrLf)

        Return sbDoc.ToString
    End Function

    Private Function getXmlEntity(ByVal strLine As String) As String

        strLine = strLine.Replace("&", "&amp;")
        strLine = strLine.Replace("<", "&lt;")
        strLine = strLine.Replace(">", "&gt;")
        'strLine = strLine.Replace("""", "&quot;")
        'strLine = strLine.Replace("'", "&apos;")

        Return strLine
    End Function

    Sub WriteXmlFile(ByVal strDoc As String, ByVal strFileNameHead As String)
        Dim strFile As String = xmlOut & "/" & strFileNameHead & "_" & Format(Now, "yyyy-MM-dd_HH-mm-ss") & ".xml"
        LogFile.WriteFile(strFile, strDoc)
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Me.TextBox1.Text = ""
        Me.TextBox2.Text = ""
        Application.DoEvents()
        Me.TextBox1.Text = GetUrlResponse(URLGrunnspill)
        Me.TextBox2.Text = LagNitf(Me.TextBox1.Text, "Resultater Norway Cup - grunnspill", "fotb-NC-grunn-res", versionNr)
        WriteXmlFile(Me.TextBox2.Text, "NorwayCup-grunnspill")
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.TextBox1.Text = ""
        Me.TextBox2.Text = ""
        Application.DoEvents()
        Me.TextBox1.Text = GetUrlResponse(URLSluttspill)
        Me.TextBox2.Text = LagNitf(Me.TextBox1.Text, "Resultater Norway Cup - sluttspill", "fotb-NC-slutt-res", versionNr)
        WriteXmlFile(Me.TextBox2.Text, "NorwayCup-sluttspill")
    End Sub

    Private Sub RunJob(ByVal type As String)
        Dim strDoc As String
        If type = "grunnspill" Then
            strDoc = GetUrlResponse(URLGrunnspill)
        Else
            strDoc = GetUrlResponse(URLSluttspill)
        End If

        If Trim(strDoc) = "" Then
            Dim strDocXml As String
            strDocXml = LagNitf(strDoc, "Resultater Norway Cup - " & type, "fotb-NC-" & type.Substring(1, 5) & "-res", versionNr)
            WriteXmlFile(strDocXml, "NorwayCup-" & type)
            XmlDoc.Save(versionFile)
        End If
    End Sub

End Class
